﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;

public class ServiceGameSave {

	private BinaryFormatter binaryFormatter =  new BinaryFormatter ();
	private string dir = "/SaveGame/";

	private void Init () {
		if (!Directory.Exists(Application.persistentDataPath+dir)) {
			Directory.CreateDirectory(Application.persistentDataPath+dir);
		}
	}

	public void Save (System.Object saveObject, string fileName) {
		FileStream file = File.Create (Application.persistentDataPath+dir+fileName+".bin");
		binaryFormatter.Serialize(file, saveObject);
		file.Close();
	}

	public System.Object Load (string fileName) {
		if (File.Exists(Application.persistentDataPath+dir+fileName+".bin")) {
			FileStream file = File.Open(Application.persistentDataPath+dir+fileName+".bin", FileMode.Open);
			System.Object saveObject = binaryFormatter.Deserialize(file);
			file.Close();

			return saveObject;
		}
		return null;
	}

	public void Reset () {
		if (Directory.Exists(Application.persistentDataPath+dir)) {
			string[] files = Directory.GetFiles (Application.persistentDataPath+dir);

			foreach (string file in files) {
				File.Delete (file);
			}
		}
	}

}
