﻿using UnityEngine;
using System.Linq;
using System;
using UniRx;

public class MessageBase {

	public MonoBehaviour sender {get; private set;}
	public int id {get; private set;}
	public System.Object data {get; private set;}

	public MessageBase (MonoBehaviour sender, int id, System.Object data) {
		this.sender = sender;
		this.id = id;
		this.data = data;
	}

	public static MessageBase Create (MonoBehaviour sender, int id, System.Object data) {
		return new MessageBase (sender, id, data);
	}
}

public static class MessageBrokerExtention {
    public static void Publish(this MonoBehaviour sender, int id, object data) {
        MessageBroker.Default.Publish(MessageBase.Create(sender, id, data));
    }

    public static void Recieve(this MonoBehaviour sender, object data, Action callback, CompositeDisposable disposable, params int[] ids) {
        MessageBroker.Default.Receive<MessageBase>().Where(msg => ids.Contains(msg.id)).Subscribe(msg => callback()).AddTo(disposable);
    }

    public static void Recieve(this MonoBehaviour sender, object data, Action callback, params int[] ids)
    {
        MessageBroker.Default.Receive<MessageBase>().Where(msg => ids.Contains(msg.id)).Subscribe(msg => callback()).AddTo(sender);
    }
    
}
