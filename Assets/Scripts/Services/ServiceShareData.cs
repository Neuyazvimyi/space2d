﻿using System;
using UnityEngine;

public class ServiceShareData {

	public const int MSG_NEED_GALAXY = 1001;
	public const int MSG_SPECIAL_MODE = 1002;
	public const int MSG_RESET_BTN = 1003;

	public const string NAME_ASTEROIDS = "asteroids";
	public const string TAG_PLANET = "Planet";

	public const string SG_MODEL_MAIN = "SG_MODEL_MAIN";

}