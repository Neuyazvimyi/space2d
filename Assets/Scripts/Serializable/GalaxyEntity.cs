﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GalaxyEntity {

	public string id;
	public int width;
	public int height;
	public VectorEntity position;
	public Cell[,] cells;
	public List<Planet> planetList;


}
