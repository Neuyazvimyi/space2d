﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class VectorEntity {

	public float x;
	public float y;
	public float z;

	public VectorEntity (Vector3 vector) {
		x = vector.x;
		y = vector.y;
		z = vector.z;
	}

	public Vector3 GetVector () {
		return new Vector3 (x, y, z);
	}

}
