﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class CellularAutomaton {

	public int width;
	public int height;

	private Cell[,] cells;
	public List<Planet> planetList;
	private System.Random random;

	public Cell[,] Create (float noise) {
		random = new System.Random ();

		cells = new Cell[width, height];
		planetList = new List<Planet> ();

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				cells [x, y] = new Cell ();
				cells[x, y].state = random.NextDouble() < noise ? 1 : 0;
			}
		}

		CalculateCells (1);
		RemoveCells ();
		AddCells ();

		return cells;
	}

	private void AddCells () {
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				int radius = 1;

				if (cells [x, y].count % 2 == 0) {
					radius = cells [x, y].count - 1;
				} else {
					radius = cells [x, y].count;
				}

				if (cells [x, y].count > 1 && CountNeighbours(x, y, radius+1, 2) == 0) {
					CreatePlanet (x, y, radius);
				}
			}
		}
	}

	private void CreatePlanet (int cellX, int cellY, int radius) {
		int minX = Mathf.Max(cellX - radius, cells.GetLowerBound(0));
		int maxX = Mathf.Min(cellX + radius, cells.GetUpperBound(0));
		int minY = Mathf.Max(cellY - radius, cells.GetLowerBound(1));
		int maxY = Mathf.Min(cellY + radius, cells.GetUpperBound(1));

		Planet planet = new Planet ();
		planet.id = planet.GetHashCode ().ToString ();
		planet.rating = random.Next (0, 10000);
		planet.position = new VectorEntity (new Vector2(cellX, cellY));

		for (int x = minX; x <= maxX; x++){
			for (int y = minY; y <= maxY; y++){
				cells [x, y].state = 2;
				cells [x, y].id = planet.id;
			}
		}

		planetList.Add (planet);
	}

	private void RemoveCells () {
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (cells [x, y].count > 1) {
					cells [x, y].state = 0;
				}
			}
		}
	}

	private void CalculateCells (int state) {
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				if (cells [x, y].state == state) {
					CountNeighbours (x, y, 1, 1);
				}
			}
		}
	}

	private int CountNeighbours (int cellX, int cellY, int radius, int state) {
		int count = 0; 

		int minX = Mathf.Max(cellX - radius, cells.GetLowerBound(0));
		int maxX = Mathf.Min(cellX + radius, cells.GetUpperBound(0));
		int minY = Mathf.Max(cellY - radius, cells.GetLowerBound(1));
		int maxY = Mathf.Min(cellY + radius, cells.GetUpperBound(1));

		for (int x = minX; x <= maxX; x++){
			for (int y = minY; y <= maxY; y++){
				if (cells [x, y].state == state) {
					count++;
				}
			}
		}

		cells [cellX, cellY].count = count;
		return count;
	}

}
