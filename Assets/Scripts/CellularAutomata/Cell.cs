﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Cell {

	public int state;
	public int count;
	public string id;

}
