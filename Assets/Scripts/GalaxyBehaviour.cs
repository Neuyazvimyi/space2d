﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System.Linq;
using Zenject;

public class GalaxyBehaviour : MonoBehaviour {

	public string id;
	private BoxCollider2D boxCollider;
	public GalaxyEntity galaxyEntity;
	public List<GameObject> planetList;
	private GameObject asteroidsParent;
	public Transform galaxyParent;
	public GameObject squarePrefab;
	public GameObject planetPrefab;

	// Use this for initialization
	void Start () {
		
	}

	public void GenerateGalaxy (string id, int size, Vector3 position, float noise) {
		galaxyEntity = new GalaxyEntity ();
		galaxyEntity.id = id;
		galaxyEntity.width = size;
		galaxyEntity.height = size;
		galaxyEntity.position = new VectorEntity (position);

		CellularAutomaton automaton = new CellularAutomaton ();

		Observable.Start (() => {
			automaton.width = size;
			automaton.height = size;

			return automaton.Create (noise);
		}).ObserveOnMainThread ().Subscribe (xs => {
			galaxyEntity.cells = xs;
			galaxyEntity.planetList = automaton.planetList;
			InitializeGalaxy ();
			FillGalaxyGrid ();
		}).AddTo (this);
	}

	private void InitializeGalaxy () {
		planetList = new List<GameObject> ();

		transform.SetParent (galaxyParent);
		transform.localPosition = galaxyEntity.position.GetVector ();
		transform.name = galaxyEntity.id;

		boxCollider = GetComponent<BoxCollider2D> ();
		boxCollider.size = new Vector2 (galaxyEntity.width, galaxyEntity.height);
		boxCollider.offset = new Vector2 (galaxyEntity.width / 2, galaxyEntity.height / 2);

		asteroidsParent = new GameObject (ServiceShareData.NAME_ASTEROIDS);
		asteroidsParent.transform.SetParent (transform);
	}

	private void FillGalaxyGrid () {
		Cell[,] cells = galaxyEntity.cells;
		Vector3 tempPos = new Vector3 (0, 0, 0);

		for (int x = 0; x < galaxyEntity.width; x++) {
			for (int y = 0; y < galaxyEntity.height; y++) {
				if (cells[x, y].state == 1) {
					tempPos.x = x;
					tempPos.y = y;

					StartCoroutine (CreateGalaxyChunk (squarePrefab, tempPos, Color.white, asteroidsParent.transform));
				}
				else if (cells[x, y].state == 2) {
					GameObject planetGo = planetList.Find (xs => xs.name == cells[x, y].id);

					if (planetGo == null) {
						planetGo =  Instantiate (planetPrefab, transform) as GameObject;
						planetGo.tag = ServiceShareData.TAG_PLANET;
						planetGo.name = cells [x, y].id;
						Planet planet = galaxyEntity.planetList.Find (xs => xs.id == planetGo.name);
						planetGo.transform.localPosition = planet.position.GetVector ();
						PlanetBehaviour planetBehaviour = planetGo.GetComponent<PlanetBehaviour> ();
						planetBehaviour.id = planet.id;
						planetBehaviour.rating = planet.rating;
						planetBehaviour.txtRating.text = planet.rating.ToString ();

						planetList.Add (planetGo);
					}

					tempPos.x = x;
					tempPos.y = y;

					StartCoroutine (CreateGalaxyChunk (squarePrefab, tempPos, Color.red, planetGo.transform));
				}
			}
		}
	}

	IEnumerator CreateGalaxyChunk (GameObject prefabGo, Vector2 posGo, Color colorGo, Transform parent) {
		yield return new WaitForEndOfFrame ();

		GameObject tempGo =  Instantiate (prefabGo, transform) as GameObject;
		tempGo.transform.localPosition = posGo;
		tempGo.transform.SetParent (parent);
		tempGo.GetComponent<SpriteRenderer> ().color = colorGo;
	}

	public void DestroyGalaxy () {
		StartCoroutine (DestroyCoroutine ());
	}

	IEnumerator DestroyCoroutine () {
		yield return new WaitForEndOfFrame ();

		Destroy (gameObject);
	}

	public void LoadGalaxy () {
		InitializeGalaxy ();
		FillGalaxyGrid ();
	}

}
