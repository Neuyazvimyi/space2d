﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class PlanetBehaviour : MonoBehaviour {

	public string id;
	public int rating;
	public TextMesh txtRating;
	public MeshRenderer meshRenderer;
	public bool isVisible;

	public CompositeDisposable disposables;

	// Use this for initialization
	void Start () {
		meshRenderer = GetComponent<MeshRenderer> ();
		isVisible = true;
	}

}
