﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

public class GameController : MonoBehaviour {

	public Transform galaxyParent;
	public GameObject squarePrefab;
	public GameObject galaxyPrefab;
	public GameObject planetPrefab;
	public Transform shipTr;

	public List<GalaxyEntity> currentGalaxyList;

	public MainModel mainModel;

	public int size = 100;
	public float noise = 0.04F;
	public int specialModeLimit = 5;

	private ServiceGameSave serviceGameSave;

	[Inject]
	public void Construct (ServiceGameSave serviceGameSave) {
		this.serviceGameSave = serviceGameSave;
	}

	void Awake () {
		InitProgress ();
	}

	// Use this for initialization
	void Start () {

	}

	private GalaxyEntity GenerateGalaxy (Vector3 position) {
		GameObject galaxyGo = Instantiate (galaxyPrefab) as GameObject;
		GalaxyBehaviour galaxyTemp = galaxyGo.GetComponent<GalaxyBehaviour> ();
		galaxyTemp.galaxyParent = galaxyParent;
		galaxyTemp.squarePrefab = squarePrefab;
		galaxyTemp.planetPrefab = planetPrefab;
		galaxyTemp.GenerateGalaxy (GenerateId (), size, position, noise);
		mainModel.galaxyList.Add(galaxyTemp.galaxyEntity);
		currentGalaxyList.Add (galaxyTemp.galaxyEntity);

		return galaxyTemp.galaxyEntity;
	}

	private void LoadGalaxy (GalaxyEntity galaxyEntity) {
		GameObject galaxyGo = Instantiate (galaxyPrefab) as GameObject;
		GalaxyBehaviour galaxyTemp = galaxyGo.GetComponent<GalaxyBehaviour> ();
		galaxyTemp.galaxyParent = galaxyParent;
		galaxyTemp.squarePrefab = squarePrefab;
		galaxyTemp.planetPrefab = planetPrefab;
		galaxyTemp.galaxyEntity = galaxyEntity;
		galaxyTemp.LoadGalaxy ();

		currentGalaxyList.Add (galaxyEntity);
	}

	public void RequestGalaxy (Vector3 position) {
		GalaxyEntity galaxyEntity = mainModel.galaxyList.Find (xs => xs.position.x == position.x && xs.position.y == position.y);

		if (galaxyEntity == null) {
			galaxyEntity = GenerateGalaxy (position);
		} else {
			GalaxyEntity currentEntity = currentGalaxyList.Find (xs => xs.position.x == position.x && xs.position.y == position.y);
			if (currentEntity == null) {
				LoadGalaxy (galaxyEntity);
			}
		}
	}

	public string GenerateId () {
		return System.Guid.NewGuid().ToString("N");
	}

	private void InitProgress () {
//		serviceGameSave.Reset ();
		currentGalaxyList = new List<GalaxyEntity> ();
		mainModel = (MainModel)serviceGameSave.Load (ServiceShareData.SG_MODEL_MAIN);

		if (mainModel == null) {
			mainModel = new MainModel ();
			mainModel.galaxyList = new List<GalaxyEntity> ();
			mainModel.shipPos = new VectorEntity (new Vector2 (size / 2, size / 2));
			mainModel.shipRating = Random.Range (0, 10000);

			GenerateGalaxy (Vector3.zero);
		} else {
			RequestGalaxy (mainModel.galaxyLast.GetVector());
		}

	}

	public void SaveProgress () {
		serviceGameSave.Save (mainModel, ServiceShareData.SG_MODEL_MAIN);
	}

	void OnApplicationQuit () {
		SaveProgress ();
	}

	void OnApplicationPause (bool pauseStatus) {
		if (pauseStatus) {
			SaveProgress ();
		}
	}

	public void ReloadScene () {
		SceneManager.LoadScene (SceneManager.GetActiveScene ().buildIndex);
		Destroy (gameObject);
	}

}
