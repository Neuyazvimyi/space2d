﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using Zenject;

public class ShipBehaviour : MonoBehaviour {

	private float genDistance;
	private Transform galaxyTr;

	public Transform shipTransform;
	private Vector3 shipPos;
	private Vector3 shipRot;

	private int moveStep = 1;

	public ReactiveProperty<float> posX { get; private set; }
	public ReactiveProperty<float> posY { get; private set; }

	private GameController gameController;

	[Inject]
	public void Construct (GameController gameController) {
		this.gameController = gameController;
	}

	// Use this for initialization
	void Start () {
		transform.localPosition = gameController.mainModel.shipPos.GetVector ();
		genDistance = gameController.size / 4;

		posX = new ReactiveProperty<float>(shipPos.x);
		posY = new ReactiveProperty<float>(shipPos.y);

		posX.ObserveEveryValueChanged (x => x.Value).Subscribe (xs => {
			if (galaxyTr != null) {
				CheckPosition ();
			}
		}).AddTo (this);
		posY.ObserveEveryValueChanged (x => x.Value).Subscribe (xs => {
			if (galaxyTr != null) {
				CheckPosition ();
			}
		}).AddTo (this);

		Observable.EveryUpdate ().Subscribe (_ => {
			OnEveryUpdate ();
		}).AddTo (this);

	}

	void OnTriggerEnter2D (Collider2D other) {
		galaxyTr = other.transform;
	}

	private void OnEveryUpdate () {
		shipPos = transform.localPosition;
		shipRot = shipTransform.localRotation.eulerAngles;

		if (Input.GetKeyDown (KeyCode.W)) {
			shipPos.y += moveStep;
			shipRot.z = 0F;
		}
		if (Input.GetKeyDown (KeyCode.S)) {
			shipPos.y -= moveStep;
			shipRot.z = 180F;
		}
		if (Input.GetKeyDown (KeyCode.A)) {
			shipPos.x -= moveStep;
			shipRot.z = 90F;
		}
		if (Input.GetKeyDown (KeyCode.D)) {
			shipPos.x += moveStep;
			shipRot.z = -90F;
		}

		transform.localPosition = shipPos;
		shipTransform.localRotation = Quaternion.Euler (shipRot);

		posX.Value = shipPos.x;
		posY.Value = shipPos.y;
	}

	private float Round (float value) {
		int snapValue = gameController.size;
		return snapValue * Mathf.Round((value / snapValue));
	}

	private void CheckPosition () {
		Vector2 tempPos = transform.position;
		Vector2 roundPos = Vector2.zero;
		roundPos.x = Round (tempPos.x);
		roundPos.y = Round (tempPos.y);

		float resX = Mathf.Abs ((long)roundPos.x - tempPos.x);
		float resY = Mathf.Abs ((long)roundPos.y - tempPos.y);

		if (resX < genDistance) {
			if (roundPos.x < tempPos.x) {
				roundPos.x -= gameController.size;
			}
			if (roundPos.y > tempPos.y) {
				roundPos.y -= gameController.size;
			}
			gameController.RequestGalaxy (roundPos);
		}
		if (resY < genDistance) {
			if (roundPos.x > tempPos.x) {
				roundPos.x -= gameController.size;
			}
			if (roundPos.y < tempPos.y) {
				roundPos.y -= gameController.size;
			}
			gameController.RequestGalaxy (roundPos);
		}

		gameController.mainModel.shipPos.x = shipPos.x;
		gameController.mainModel.shipPos.y = shipPos.y;
		gameController.mainModel.galaxyLast = new VectorEntity(galaxyTr.localPosition);
	}



}
