﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;

public class CameraBehaviour : MonoBehaviour {

	public Transform shipTr;
	public float dampening = 10.0f;

	private Vector3 camPos;
	private float currentZoom;
	private Camera mainCam;

	private float zoomMin = 5F;
	private float zoomMax = 30F;
	private float zoomSensitivity = 20F;
	private float specialMode = 20F;

	public ReactiveProperty<bool> isSpecialMode { get; private set; }

	// Use this for initialization
	void Start () {
		mainCam = GetComponent<Camera> ();
		currentZoom = mainCam.orthographicSize;
		isSpecialMode = new ReactiveProperty<bool> (false);

		isSpecialMode.ObserveEveryValueChanged (x => x.Value).Subscribe (xs => {
			if (xs) {
				MessageBroker.Default.Publish (MessageBase.Create (this, ServiceShareData.MSG_SPECIAL_MODE, true));
			}
			else {
				MessageBroker.Default.Publish (MessageBase.Create (this, ServiceShareData.MSG_SPECIAL_MODE, false));
			}
		}).AddTo (this);
	}
	
	// Update is called once per frame
	void LateUpdate () {
		camPos = Vector3.Lerp(transform.position, shipTr.position, dampening);
		camPos.z = -10F;
		transform.position = camPos;

		MouseScrollWheel ();
	}

	private void MouseScrollWheel () {
		if (Input.GetAxis("Mouse ScrollWheel") > 0) {
			float factor = dampening * zoomSensitivity * Time.deltaTime;
			float zoom = zoomMin;
			currentZoom = Mathf.Lerp(currentZoom, zoom, factor);
			mainCam.orthographicSize = currentZoom;
		}
		if (Input.GetAxis("Mouse ScrollWheel") < 0) {
			float factor = dampening * zoomSensitivity * Time.deltaTime;
			float zoom = zoomMax;
			currentZoom = Mathf.Lerp(currentZoom, zoom, factor);
			mainCam.orthographicSize = currentZoom;
		}
		if (currentZoom > specialMode) {
			isSpecialMode.Value = true;	
		} else {
			isSpecialMode.Value = false;
		}

	}

}
