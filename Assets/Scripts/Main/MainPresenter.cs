﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UniRx;
using System.Linq;
using Unity.Linq;
using Zenject;

public class MainPresenter : MonoBehaviour {

	public MainView mainView;
	public GameObject spaceGo;

	public CompositeDisposable disposables;

	private GameController gameController;
	private ServiceGameSave serviceGameSave;

	[Inject]
	public void Construct (GameController gameController, ServiceGameSave serviceGameSave) {
		this.gameController = gameController;
		this.serviceGameSave = serviceGameSave;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	void OnEnable () {
		disposables = new CompositeDisposable();
		MessageBroker.Default.Receive<MessageBase>().Where(msg => msg.id == ServiceShareData.MSG_SPECIAL_MODE).Subscribe(msg => OnMessage(msg)).AddTo (disposables);
		MessageBroker.Default.Receive<MessageBase>().Where(msg => msg.id == ServiceShareData.MSG_RESET_BTN).Subscribe(msg => OnMessage(msg)).AddTo (disposables);
	}

	void OnDisable () {
		if (disposables != null) {
			disposables.Dispose ();
		} 
	}

	public void OnMessage (MessageBase msg) {
		if (msg.id == ServiceShareData.MSG_SPECIAL_MODE) {
			bool isSpecialMode = (bool)msg.data;

			if (isSpecialMode) {
				List<PlanetBehaviour> nearestPlanets = spaceGo.Descendants ().OfComponent<PlanetBehaviour>()
					.Where (xs => xs.tag == ServiceShareData.TAG_PLANET && xs.meshRenderer.isVisible)
					.OrderBy (xs => Mathf.Abs((long) xs.rating - gameController.mainModel.shipRating))
					.Take (gameController.specialModeLimit)
					.ToList<PlanetBehaviour> ();

				List<PlanetBehaviour> planetList = spaceGo.Descendants ().OfComponent<PlanetBehaviour>()
					.Where (xs => xs.tag == ServiceShareData.TAG_PLANET && xs.meshRenderer.isVisible)
					.Except<PlanetBehaviour> (nearestPlanets)
					.ToList<PlanetBehaviour> ();

				for (int i=0; i<planetList.Count; i++) {
					planetList [i].isVisible = false;
					planetList [i].gameObject.SetActive (false);
				}
			} else {
				List<PlanetBehaviour> planetList = spaceGo.Descendants ().OfComponent<PlanetBehaviour>()
					.Where (xs => xs.tag == ServiceShareData.TAG_PLANET && !xs.isVisible)
					.ToList<PlanetBehaviour> ();

				for (int i=0; i<planetList.Count; i++) {
					planetList [i].gameObject.SetActive (true);
				}
			}
		} else if (msg.id == ServiceShareData.MSG_RESET_BTN) {
			serviceGameSave.Reset ();
			gameController.ReloadScene ();
		}

	}



}
