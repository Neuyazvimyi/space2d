﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UniRx;

public class MainView : MonoBehaviour {

	public Button resetBtn;

	// Use this for initialization
	void Start () {
		resetBtn.OnClickAsObservable ().Subscribe (_ => {
			MessageBroker.Default.Publish (MessageBase.Create (this, ServiceShareData.MSG_RESET_BTN, null));	
		});
	}

}
