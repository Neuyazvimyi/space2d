﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class MainModel {

	public VectorEntity shipPos;
	public List<GalaxyEntity> galaxyList;
	public VectorEntity galaxyLast;
	public int shipRating;

}
