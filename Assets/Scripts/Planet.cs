﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Planet {

	public string id;
	public int rating;
	public VectorEntity position;

}
