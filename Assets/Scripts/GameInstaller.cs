using UnityEngine;
using Zenject;

public class GameInstaller : MonoInstaller<GameInstaller>{
    
	public override void InstallBindings(){
		InstallGame ();
    }

	void InstallGame () {
		Container.Bind<ServiceGameSave> ().FromInstance (new ServiceGameSave ()).AsSingle ();
	}



}